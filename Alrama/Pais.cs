﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alrama
{
    class Pais
    {
        public Pais(String nom)
        {
            this.NomPais = nom;
        }

        public String NomPais { get; set; }
    }
}
